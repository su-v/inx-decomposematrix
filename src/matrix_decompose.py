#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
matrix_decompose - matrix decomposition

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Incn
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Notes on transformations

    Transformations in SVG 1.1:

        * Functional notation:
        matrix(<a> <b> <c> <d> <e> <f>), which specifies a transformation in
        the form of a transformation matrix of six values. matrix(a,b,c,d,e,f)
        is equivalent to applying the transformation matrix [a b c d e f].

        matrix(a,b,c,d,e,f)

        * Coordinate system transformations:
        Transformations map coordinates and lengths from a new coordinate
        system into a previous coordinate system:
        ┌        ┐    ┌       ┐   ┌       ┐
        | x_prev |    | a c e |   | x_new |
        | y_prev | =  | b d f | * | y_new |
        | 1      |    | 0 0 1 |   | 1     |
        └        ┘    └       ┘   └       ┘

    Transformations in CSS3 / SVG 2:

        * Functional notation:
        The matrix() CSS function specifies a homogeneous 2D transformation
        matrix comprised of the specified six values. The constant values of
        such matrices are implied and not passed as parameters; the other
        parameters are described in the column-major order.

        matrix(a, b, c, d, tx, ty) is a shorthand for
        matrix3d(a, b, 0, 0, c, d, 0, 0, 0, 0, 1, 0, tx, ty, 0, 1).

        The matrix3d() CSS function describes a 3D transform as a 4x4
        homogeneous matrix. The 16 parameters are described in the column-major
        order.

        * 2D matrix:
        A 3x2 transformation matrix (in column-major order) with 6 items or a
        4x4 matrix (in column-major order) with 16 items, where the items m31,
        m32, m13, m23, m43, m14, m24, m34 are equal to 0 and m33, m44 are equal
        to 1.
            ┌             ┐   ┌       ┐
        M = | m11 m21 m31 | = | a c e |
            | m12 m22 m32 |   | b d f |
            └             ┘   └       ┘
            ┌                 ┐   ┌         ┐
            | m11 m21 m31 m41 |   | a c 0 e |
        M = | m12 m22 m32 m42 | = | b d 0 f |
            | m13 m23 m33 m43 |   | 0 0 1 0 |
            | m14 m24 m34 m44 |   | 0 0 0 1 |
            └                 ┘   └         ┘
        * Mathematical Description of Transform Functions:
        Mathematically, all transform functions can be represented as 4x4
        transformation matrices of the following form:
            ┌                 ┐
            | m11 m21 m31 m41 |
        M = | m12 m22 m32 m42 |
            | m13 m23 m33 m43 |
            | m14 m24 m34 m44 |
            └                 ┘
        A 2D 3x2 matrix with six parameters a, b, c, d, e and f is equivalent
        to the matrix:
            ┌         ┐
            | a c 0 e |
        M = | b d 0 f |
            | 0 0 1 0 |
            | 0 0 0 1 |
            └         ┘

        * Two Dimensional Subset:
        The mathematical description of transform functions is still effective
        but can be reduced by using a 3x3 transformation matrix where a equals
        m11, b equals m12, c equals m21, d equals m22, e equals m41 and f
        equals m42.
            ┌       ┐
            | a c e |
        M = | b d f |
            | 0 0 1 |
            └       ┘

"""
# standard library
import math
from copy import deepcopy

# inkscape library
import inkex
from simpletransform import parseTransform, formatTransform
from simpletransform import invertTransform, composeTransform


# globals
EPSILON = 1e-5
# inkscape's simpletransform.py uses row-major order
IDENT_2DINX = [[1.0, 0.0, 0.0],
               [0.0, 1.0, 0.0]]
IDENT_3DCSS = [[1.0, 0.0, 0.0, 0.0],
               [0.0, 1.0, 0.0, 0.0],
               [0.0, 0.0, 1.0, 0.0],
               [0.0, 0.0, 0.0, 1.0]]


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Test approximate equality.

    ref:
        PEP 485 -- A Function for testing approximate equality
        https://www.python.org/dev/peps/pep-0485/#proposed-implementation
    """
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def decompose_2d_transform(mat, method='QR', tlist=None):
    """Decompose 2x3 row-major order matrix based on chosen method.

    Default order is 'row-major' (2x3 matrix from simpletransform.py):
              ┌       ┐
        mat = | a c e | = [[a, c, e],
              | b d f |    [b, d, f]]
              └       ┘

    Set order to 'column-major' for 4x4 input matrix based on CSS Transforms:
              ┌         ┐
        mat = | a c 0 e | = [[a, b, 0, 0],
              | b d 0 f |    [c, d, 0, 0],
              | 0 0 1 0 |    [0, 0, 1, 0]
              | 0 0 0 1 |    [e, f, 0, 1]]
              └         ┘

    References:

    LU, QR:
        'Decomposition of 2D-transform matrices'
        http://frederic-wang.fr/decomposition-of-2d-transform-matrices.html

    SVD:
        'Decompose a 2D arbitrary transform into only scaling and rotation'
        http://math.stackexchange.com/q/861674

    CSS Firefox:
        Standard representation of translate, rotate, x-skew and scale as
        implemented in Firefox and e.g. d3:
        http://hg.mozilla.org/mozilla-central/file/tip/layout/style/StyleAnimationValue.cpp#l1534
        https://github.com/d3/d3-interpolate/blob/master/src/transform/decompose.js
        CSS 2D Transforms - W3C Working Draft 15 December 2011
        https://www.w3.org/TR/2011/WD-css3-2d-transforms-20111215/#matrix-decomposition

    CSS WebKit:
        The current CSS Transforms spec has pseudo code to decompose (and
        recompose to) a 2D matrix based on WebKit's implementation:
        https://trac.webkit.org/browser/trunk/Source/WebCore/platform/graphics/transforms/TransformationMatrix.cpp#L296
        CSS Transforms Module Level 1 - W3C Working Draft, 26 November 2013
        https://www.w3.org/TR/css-transforms-1/#decomposing-a-2d-matrix

        Based on tests this decomposition seems only useful for interpolation
        (animation) and subsequent recomposition as described in the spec - a
        regular composition of the decomposed basic transform types does not
        necessarily produce the expected identical result.

        This decomposition method is currently disabled in the GUI.

    """
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    # pylint: disable=unused-variable

    # In this implementation, the input matrix is defined in row-major order.
    order = 'row-major'

    def _translate(tx, ty):
        tlist.append(['translate', tx, ty])

    def _scale(sx, sy):
        tlist.append(['scale', sx, sy])

    def _rotate(a):
        tlist.append(['rotate', math.degrees(a)])

    def _skew_x(a):
        tlist.append(['skewX', math.degrees(a)])

    def _skew_y(a):
        tlist.append(['skewY', math.degrees(a)])

    def _skew_matrix(a, b, c, d):
        tlist.append(['matrix', a, b, c, d])

    def _matrix(a=1.0, b=0.0, c=0.0, d=1.0, e=0.0, f=0.0):
        # pylint: disable=too-many-arguments
        tlist.append(['matrix', a, b, c, d, e, f])

    # check arguments
    if tlist is None:
        tlist = []
    elif not isinstance(tlist, list):
        return  # wrong argument type, do nothing
    if order == 'row-major':  # 2x3 matrix as returned by simpletransform.py
        if not mat:
            mat = deepcopy(IDENT_2DINX)
        elif len(mat) != 2 or len(mat[0]) != 3:
            return tlist  # incompatible matrix, return current list
    else:  # 2D transform as 4x4 matrix in column-order
        if not mat:
            mat = deepcopy(IDENT_3DCSS)
        elif len(mat) != 4 or len(mat[0]) != 4:
            return tlist  # incompatible matrix, return current list

    # matrix components
    if order == 'row-major':  # 2x3 matrix as returned by simpletransform.py
        a = mat[0][0]
        b = mat[1][0]
        c = mat[0][1]
        d = mat[1][1]
        e = mat[0][2]
        f = mat[1][2]
    else:  # 2D transform as 4x4 matrix in column-order
        a = mat[0][0]
        b = mat[0][1]
        c = mat[1][0]
        d = mat[1][1]
        e = mat[3][0]
        f = mat[3][1]

    # determinant
    det = a*d - b*c

    # decompose translation
    _translate(e, f)

    # decompose linear transformation
    if method == 'QR':
        if a != 0 or b != 0:
            r = math.sqrt(a*a + b*b)
            theta = math.acos(a/r) if b > 0 else -math.acos(a/r)
            _rotate(theta)
            _scale(r, det/r)
            _skew_x(math.atan((a*c + b*d)/(r*r)))
        elif c != 0 or d != 0:
            s = math.sqrt(c*c + d*d)
            theta = math.acos(-c/s) if d > 0 else -math.acos(c/s)
            _rotate(math.pi/2.0 - theta)
            _scale(det/s, s)
            _skew_y(math.atan(a*c + b*d)/(s*s))
        else:  # a = b = c = d = 0
            _scale(0.0, 0.0)
    elif method == 'LU':
        if a != 0:
            _skew_y(math.atan(b/a))
            _scale(a, det/a)
            _skew_x(math.atan(c/a))
        elif b != 0:
            _rotate(math.pi / 2.0)
            _scale(b, det/b)
            _skew_x(math.atan(d/b))
        else:  # a = b = 0
            _scale(c, d)
            _skew_x(math.pi / 4.0)
            _scale(0.0, 1.0)
    elif method == 'SVD':
        if det != 0:
            # FIXME: do not overwrite original e, f
            e = (a + d) / 2.0
            f = (a - d) / 2.0
            g = (b + c) / 2.0
            h = (b - c) / 2.0
            q = math.sqrt(e*e + h*h)
            r = math.sqrt(f*f + g*g)
            sx = q + r
            sy = q - r
            a1 = math.atan2(g, f)
            a2 = math.atan2(h, e)
            theta = (a2 - a1) / 2.0
            phi = (a2 + a1) / 2.0
            _rotate(phi)
            _scale(sx, sy)
            _rotate(theta)
    elif method == 'CSS_firefox':
        if det != 0:
            scale_x = math.sqrt(a*a + b*b)
            if scale_x:
                a /= scale_x
                b /= scale_x
            skew_x = a*c + b*d
            if skew_x:
                c -= a*skew_x
                d -= b*skew_x
            scale_y = math.sqrt(c*c + d*d)
            if scale_y:
                c /= scale_y
                d /= scale_y
                skew_x /= scale_y
            if a*d < b*c:
                a = -a
                b = -b
                skew_x = -skew_x
                scale_x = -scale_x
            _rotate(math.atan2(b, a))
            _skew_x(math.atan(skew_x))
            _scale(scale_x, scale_y)
    elif method == 'CSS_webkit':
        if det != 0:

            row0x = a
            row0y = b
            row1x = c
            row1y = d

            # translate = [0.0, 0.0]
            # translate[0] = e
            # translate[1] = f

            scale = [1.0, 1.0]
            scale[0] = math.sqrt(row0x * row0x + row0y * row0y)
            scale[1] = math.sqrt(row1x * row1x + row1y * row1y)

            # If determinant is negative, one axis was flipped.
            determinant = row0x * row1y - row0y * row1x
            if determinant < 0:
                # Flip axis with minimum unit vector dot product.
                if row0x < row1y:
                    scale[0] = -scale[0]
                else:
                    scale[1] = -scale[1]

            # Renormalize matrix to remove scale.
            if scale[0]:
                row0x *= 1.0 / scale[0]
                row0y *= 1.0 / scale[0]
            if scale[1]:
                row1x *= 1.0 / scale[1]
                row1y *= 1.0 / scale[1]

            # Compute rotation and renormalize matrix.
            angle = math.atan2(row0y, row0x)

            if angle:
                # Rotate(-angle) = [cos(angle), sin(angle), -sin(angle), cos(angle)]
                #                = [row0x, -row0y, row0y, row0x]
                # Thanks to the normalization above.
                sn = -row0y
                cs = row0x
                m11 = row0x
                m12 = row0y
                m21 = row1x
                m22 = row1y
                row0x = cs * m11 + sn * m21
                row0y = cs * m12 + sn * m22
                row1x = -sn * m11 + cs * m21
                row1y = -sn * m12 + cs * m22

            m11 = row0x
            m12 = row0y
            m21 = row1x
            m22 = row1y

            # # Convert into degrees because our rotation functions expect it.
            # angle = math.degrees(angle)

            _scale(scale[0], scale[1])
            _skew_matrix(m11, m12, m21, m22)
            _rotate(angle)

    else:
        tlist = []

    return tlist


def format_raw_transforms(tlist):
    """Format ordered list with decomposed transform values."""
    formatted_list = []
    if len(tlist) and isinstance(tlist, list):
        for item in tlist:
            key = item.pop(0)
            transform = '{}({})'.format(key, ','.join(str(v) for v in item))
            if transform:
                formatted_list.append(transform)
    return formatted_list


def recompose_2d_transform(tlist, method='QR'):
    """Recompose decomposed 2d transformation into a single matrix."""
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    # In this implementation, the input matrix is defined in row-major order.
    order = 'row-major'

    def _compose_transforms(tlist):
        """Compose transforms with simpletransform."""
        formatted_list = format_raw_transforms(tlist)
        mat = parseTransform(formatted_list.pop(0))
        for item in formatted_list:
            mat = parseTransform(item, mat)
        return formatTransform(mat)

    # check arguments
    if not len(tlist):
        return
    if order == 'row-major':
        # 2x3 matrix as returned by simpletransform.py
        mat = deepcopy(IDENT_2DINX)
    else:
        # 2D transform as 4x4 matrix in column-order
        mat = deepcopy(IDENT_3DCSS)

    if method == 'QR':
        transform = _compose_transforms(tlist)
    elif method == 'LU':
        transform = _compose_transforms(tlist)
    elif method == 'SVD':
        transform = _compose_transforms(tlist)
    elif method == 'CSS_firefox':
        transform = _compose_transforms(tlist)
    elif method == 'CSS_webkit':

        for item in tlist:
            key = item.pop(0)
            if key == 'translate':
                translate = item
            elif key == 'scale':
                scale = item
            elif key == 'rotate':
                angle = item[0]
            elif key == 'matrix':
                m11, m12, m21, m22 = item[:4]
            else:
                inkex.debug(item)

        if order == 'row-major':
            mat[0][0] = m11  # a
            mat[1][0] = m12  # b
            mat[0][1] = m21  # c
            mat[1][1] = m22  # d
        else:
            mat[0][0] = m11  # a
            mat[0][1] = m12  # b
            mat[1][0] = m21  # c
            mat[1][1] = m22  # d

        # Translate matrix.
        # https://github.com/w3c/csswg-drafts/issues/483
        workaround = True
        if order == 'row-major':
            if not workaround:
                mat[0][2] = translate[0] * m11 + translate[1] * m21
                mat[1][2] = translate[0] * m12 + translate[1] * m22
            else:
                mat[0][2] = translate[0]
                mat[1][2] = translate[1]
        else:
            if not workaround:
                mat[3][0] = translate[0] * m11 + translate[1] * m21
                mat[3][1] = translate[0] * m12 + translate[1] * m22
            else:
                mat[3][0] = translate[0]
                mat[3][1] = translate[1]

        # Rotate matrix.
        angle = math.radians(angle)
        cos_angle = math.cos(angle)
        sin_angle = math.sin(angle)

        # New temporary, identity initialized, 4x4 matrix rotateMatrix
        if order == 'row-major':
            rotate_mat = deepcopy(IDENT_2DINX)
            rotate_mat[0][0] = cos_angle
            rotate_mat[1][0] = sin_angle
            rotate_mat[0][1] = -sin_angle
            rotate_mat[1][1] = cos_angle
        else:
            rotate_mat = deepcopy(IDENT_3DCSS)
            rotate_mat[0][0] = cos_angle
            rotate_mat[0][1] = sin_angle
            rotate_mat[1][0] = -sin_angle
            rotate_mat[1][1] = cos_angle

        mat = composeTransform(mat, rotate_mat)

        # Scale matrix.
        if order == 'row-major':
            mat[0][0] *= scale[0]
            mat[1][0] *= scale[0]
            mat[0][1] *= scale[1]
            mat[1][1] *= scale[1]
        else:
            mat[0][0] *= scale[0]
            mat[0][1] *= scale[0]
            mat[1][0] *= scale[1]
            mat[1][1] *= scale[1]

        transform = formatTransform(mat)

    else:
        transform = ""

    return transform


def format_float(f):
    """Format rounded float as string, with trailing zeros stripped."""
    if isinstance(f, float):
        f_rounded = int(round(f))
        if isclose(f, f_rounded, rel_tol=EPSILON):
            f = f_rounded
        return format(f, 'f').rstrip('0').rstrip('.')
    else:
        return str(f)


def format_translate(tx, ty):
    """Format SVG translate() transformation as string."""
    tx = format_float(tx)
    ty = format_float(ty)
    if not (tx == 0 and ty == 0):
        if ty == 0:
            return 'translate({})'.format(tx)
        else:
            return 'translate({},{})'.format(tx, ty)


def format_scale(sx, sy):
    """Format SVG scale() transformation as string."""
    sx = format_float(sx)
    sy = format_float(sy)
    if not (sx == 1 and sy == 1):
        if sx == sy:
            return 'scale({})'.format(sx)
        else:
            return 'scale({},{})'.format(sx, sy)


def format_rotate(a):
    """Format SVG rotate() transformation as string."""
    if a != 0:
        angle = format_float(a)
        return 'rotate({})'.format(angle)


def format_skew_x(a):
    """Format SVG skewX() transformation as string."""
    if a != 0:
        angle = format_float(a)
        return 'skewX({})'.format(angle)


def format_skew_y(a):
    """Format SVG skewY() transformation as string."""
    if a != 0:
        angle = format_float(a)
        return 'skewY({})'.format(angle)


def format_matrix(a=1.0, b=0.0, c=0.0, d=1.0, e=0.0, f=0.0):
    """Format SVG matrix() transformation as string."""
    # pylint: disable=too-many-arguments
    return 'matrix({},{},{},{},{},{})'.format(
        format_float(a), format_float(b),
        format_float(c), format_float(d),
        format_float(e), format_float(f))


def format_transforms(tlist):
    """Format ordered list with decomposed transform values."""
    formatted_list = []
    if tlist and isinstance(tlist, list):
        for item in tlist:
            key = item.pop(0)
            if key == 'translate':
                transform = format_translate(*item)
            elif key == 'scale':
                transform = format_scale(*item)
            elif key == 'rotate':
                transform = format_rotate(*item)
            elif key == 'skewX':
                transform = format_skew_x(*item)
            elif key == 'skewY':
                transform = format_skew_y(*item)
            elif key == 'matrix':
                transform = format_matrix(*item)
            if transform is not None:
                formatted_list.append(transform)
    return formatted_list


def create_group():
    """Return a new SVG group element."""
    return inkex.etree.Element(inkex.addNS('g', 'svg'))


def del_attrib(node, key):
    """Delete attribute *key* from node if present."""
    if key is not None and key in node.attrib:
        del node.attrib[key]


def splitwrap_transform(node, tlist):
    """Apply decomposed transforms separately to wrap groups."""
    child = node
    parent = node.getparent()
    index = node.getparent().index(child)
    del_attrib(node, 'transform')
    for transf in reversed(tlist):
        group = create_group()
        group.set('transform', transf)
        group.append(child)
        child = group
    parent.insert(index, child)


def update_transform(node, tlist):
    """Apply decomposed transforms to node."""
    transforms = ' '.join(tlist)
    node.set('transform', transforms)


class DecomposeTransform(inkex.Effect):
    """Factorize matrix transforms into components."""

    def __init__(self):
        """Init base class and DecomposeTransform()."""
        inkex.Effect.__init__(self)

        # selection
        self.OptionParser.add_option("--replace",
                                     action="store", type="inkbool",
                                     dest="replace", default=True,
                                     help="Replace transform")
        self.OptionParser.add_option("--splitwrap",
                                     action="store", type="inkbool",
                                     dest="splitwrap", default=False,
                                     help="Split-wrap in nested groups")
        self.OptionParser.add_option("--recursive",
                                     action="store", type="inkbool",
                                     dest="recursive", default=False,
                                     help="Process selection recursively")
        self.OptionParser.add_option("--verbose",
                                     action="store", type="inkbool",
                                     dest="verbose", default=False,
                                     help="Verbose mode")
        # custom input
        self.OptionParser.add_option("--transform",
                                     action="store", type="string",
                                     dest="transform", default="",
                                     help="Custom transform string")
        # common options
        self.OptionParser.add_option("--method",
                                     action="store", type="string",
                                     dest="method", default="QR",
                                     help="Decomposition method")
        self.OptionParser.add_option("--invert",
                                     action="store", type="inkbool",
                                     dest="invert", default=False,
                                     help="Invert matrix first")
        self.OptionParser.add_option("--recompose",
                                     action="store", type="inkbool",
                                     dest="recompose", default=False,
                                     help="Recompose decomposition")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab", default="options",
                                     help="Selected tab")
        self.OptionParser.add_option("--t_input",
                                     action="store", type="string",
                                     dest="t_input", default="t_selection",
                                     help="Scope")

    def decompose_matrix(self, mat):
        """Decompose matrix based on Frederic Wang's blog post."""
        method = self.options.method
        recompose = self.options.recompose
        raw_list = decompose_2d_transform(mat, method)
        if recompose:
            mat = recompose_2d_transform(raw_list, method)
            return [mat]
        return format_transforms(raw_list)

    def process_transform(self, transform, verbose=False):
        """Process transform passed as string, return tlist."""
        invert = self.options.invert
        mat = []
        tlist = []
        if isinstance(transform, str) and transform:
            mat = parseTransform(transform)
        if len(mat) == 2 and len(mat[0]) == 3:
            if invert:
                mat_orig = list(mat)
                mat = invertTransform(mat_orig)
            tlist = self.decompose_matrix(mat)
        if len(tlist) and verbose:
            inkex.debug(transform)
            if invert and mat_orig:
                inkex.debug(mat_orig)
            inkex.debug(mat)
            inkex.debug(tlist)
            inkex.debug(' '.join(tlist))
        return tlist

    def process_input(self):
        """Process transform string from custom input."""
        transform = self.options.transform
        verbose = True
        if transform:
            self.process_transform(transform, verbose)

    def process_node(self, node):
        """Process transform attribute of node."""
        transform = node.get('transform')
        verbose = self.options.verbose
        tlist = []
        if transform:
            tlist = self.process_transform(transform, verbose)
        if len(tlist):
            if self.options.replace:
                if self.options.splitwrap:
                    splitwrap_transform(node, tlist)
                else:
                    update_transform(node, tlist)

    def process_selection(self, node):
        """Recursively process node from selection."""
        for child in node:
            self.process_selection(child)
        self.process_node(node)

    def effect(self):
        """Decompose matrix transform of selected objects or input."""
        if self.options.t_input == '"t_custom"':
            self.process_input()
        elif self.options.t_input == '"t_selection"':
            for node in self.selected.values():
                if self.options.recursive:
                    self.process_selection(node)
                else:
                    self.process_node(node)


if __name__ == '__main__':
    ME = DecomposeTransform()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
