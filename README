inx-decomposematrix
===================

An Inkscape extension to decompose a matrix() transformation into
individual transforms.

Basic operation:
The extension replaces a matrix() transformation of a selected object
with a composition of basic transforms. Basic transform types in SVG
are: translate, rotate, scale, skewX, skewY. This operation does not
change the final result of the transformation applied to the selected
object.


Available Options
=================

Input Selection:
The extension processes the 'transform' attribute of one or more
selected objects. Available options are:
- Replace: basic operation
- Split-wrap: apply the decomposed basic transforms individually to
  (nested) groups wrapped around the original object.
- Recursive: recursively process objects in the current selection. This
  allows to rewrite the transforms inside (possibly nested) groups in
  the current selection.
- Verbose: returns the input and output values via stderr. If 'Replace'
  is unchecked, this option can be used to extract information for
  external use without affecting the selected objects.
- Invert: invert the current transformation matrix before decomposing it
  into individual basic transform types. Note that applying the
  transforms of an inverted matrix will change the transformed output
  of the selected objects.

Input Custom:
The extension can be used to decompose arbitrary matrices pasted into a
text entry in the 'Custom' tab of the extension dialog. This operation
also supports the 'Invert' option.


Decomposition Methods
=====================

Possible outputs based on a chosen method are listed in the 'About' tab
of the extension dialog.

Currently supported decomposition methods:
- QR, LU decomposition: see this blog entry for a description:
  'Decomposition of 2D-transform matrices'
  http://frederic-wang.fr/decomposition-of-2d-transform-matrices.html
- SVD: see this question on stackexchange for a description:
  'Decompose a 2D arbitrary transform into only scaling and rotation'
  http://math.stackexchange.com/q/861674
- CSS3 2d transforms: standard representation of translate, rotate,
  x-skew and scale for animation. Based on the d3 implementation of an
  earlier version of the CSS3 2D transforms specification:
  https://github.com/d3/d3-interpolate#interpolateTransformSvg
  https://www.w3.org/TR/2011/WD-css3-2d-transforms-20111215/#matrix-decomposition


Installation
============

Copy the two files in the 'src/' directory into the directory listed as
'User extensions:' in Inkscape's 'Preferences > System' and relaunch
Inkscape.

The extension will be available as:
  Extensions > Debug > Decompose Matrix


Source
======

The extension is developed and maintained in:
https://gitlab.com/su-v/inx-decomposematrix

ZIP archives:
**NA**

Related feature requests for Inkscape:
**NA**

Examples:
**NA**


License
=======

Same as Inkscape used to be (GPL-2+).
